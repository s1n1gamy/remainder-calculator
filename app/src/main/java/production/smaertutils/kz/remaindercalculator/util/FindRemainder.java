package production.smaertutils.kz.remaindercalculator.util;

/**
 * Created by Madi on 26.01.2017.
 */
public class FindRemainder {
    public FindRemainder() {
    }

    public int findRem(int a, int b) {
        int result = 0;
        if (a > 0) {
            result = a % b;
        } else if (a < 0) {
            int buf = a * (-1);
            while (buf % b != 0) {
                buf += 1;
            }
            result = a * (-1) - buf;
        }
        return result * (-1);
    }

    public int findWithoutRemainder(int a, int b) {
        int buf=a;
        if (a > 0) {
            while (buf % b != 0) {
                buf -= 1;
            }
            return buf/b;
        } else if (a < 0) {
            buf*=(-1);
            while (buf % b != 0) {
                buf += 1;
            }
            return buf/b;
        }
        else {
            return 0;
        }
    }

}
