package production.smaertutils.kz.remaindercalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import production.smaertutils.kz.remaindercalculator.util.FindRemainder;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final FindRemainder findRemainder = new FindRemainder();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button button = (Button) findViewById(R.id.count);

        final EditText editTextNumber = (EditText) findViewById(R.id.numberToDivideET);
        final EditText editTextResult = (EditText) findViewById(R.id.resultET);
        final EditText editTextRemainder = (EditText) findViewById(R.id.remainderET);
        final EditText editTextDivider = (EditText) findViewById(R.id.dividerET);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String a = editTextNumber.getText().toString();
                String b = editTextDivider.getText().toString();

                int aInt = Integer.parseInt(a);
                int bInt = Integer.parseInt(b);
                if (a.isEmpty() || b.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Вы не ввели числа или ввели их неправильно", Toast.LENGTH_LONG).show();
                } else if (Integer.parseInt(b) == 0) {
                    Toast.makeText(getApplicationContext(), "Нельзя делить числа на ноль. Введите числа снова", Toast.LENGTH_LONG).show();
                } else {
                    int result;


                    if (aInt % bInt == 0) {
                        result=aInt/bInt;
                        editTextResult.setText(String.valueOf(result));
                        editTextRemainder.setText("0");
                    } else {
                        editTextResult.setText(String.valueOf(findRemainder.findWithoutRemainder(aInt,bInt)));
                        editTextRemainder.setText(String.valueOf(findRemainder.findRem(aInt,bInt)));
                        Toast.makeText(getApplicationContext(), "Расчитано", Toast.LENGTH_LONG).show();
                    }
                }

            }
        };
        button.setOnClickListener(onClickListener);
    }


}
